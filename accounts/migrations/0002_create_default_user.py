from django.apps import AppConfig
from django.contrib.auth.hashers import make_password
from django.db import migrations


def create_default_user(apps: AppConfig, _) -> None:
    User = apps.get_model("accounts", "User")  # noqa
    User.objects.get_or_create(
        email="default.user@firebird.com",
        username="default",
        password=make_password("default.password"),
        is_staff=True,
        is_superuser=True,
    )


def remove_default_user(apps: AppConfig, _) -> None:
    User = apps.get_model("accounts", "User")  # noqa
    default_user = User.objects.filter(username="default").first()

    if default_user is not None:
        default_user.delete()


class Migration(migrations.Migration):
    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_default_user, reverse_code=remove_default_user)
    ]
