from django.apps import AppConfig
from django.db import migrations


def create_default_genders(apps: AppConfig, _) -> None:
    Gender = apps.get_model("accounts", "Gender")  # noqa
    Gender.objects.get_or_create(id=1, value="Male")
    Gender.objects.get_or_create(id=2, value="Female")


def remove_default_genders(apps: AppConfig, _) -> None:
    Gender = apps.get_model("accounts", "Gender")  # noqa
    male = Gender.objects.filter(value="Male").first()
    female = Gender.objects.filter(value="Female").first()

    if male is not None:
        male.delete()

    if female is not None:
        female.delete()


class Migration(migrations.Migration):
    dependencies = [
        ('accounts', '0003_gender'),
    ]

    operations = [
        migrations.RunPython(create_default_genders, reverse_code=remove_default_genders)
    ]
